# About
This is R code to generate upshifted national and state-level estimates for the BRFSS data generated as part of the CASCADE [CASCADE project](https://www.sheffield.ac.uk/cascade) and SIMAH project and for a publication

Publication title: Improved estimates for individual and population-level alcohol use in the United States, 1984-2020

Authors: Charlotte Buckley, Alan Brennan, William C Kerr, Charlotte Probst, Klajdi Puka, Robin C Purshouse, Jürgen Rehm

This repo is hereby licensed for use under the GNU GPL version 3.

# Set up and run the code

In the main script that performs the adjustment procedure on BRFSS data a dummy dataset is generated so the script can be run as an example
Note that this DOES NOT produce the correct figures reported in the manuscript 

To reproduce the results in the manuscript the raw BRFSS data must be downloaded from here https://www.cdc.gov/brfss/annual_data/annual_data.htm
This data must be downloaded in .XPT format and entered into a folder on your local machine at the same level as this repository called "raw_data"

The alcohol per capita sales data must also be downloaded from here: https://pubs.niaaa.nih.gov/publications/surveillance119/CONS20.htm
This data must be entered into a folder on your local machine at the same level as this repository called "processed_data"

Contact for any questions about this repository: Charlotte Buckley, c.m.buckley@sheffield.ac.uk
